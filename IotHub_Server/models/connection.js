var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const connectionSchema = new Schema({
    connected: Boolean,
    clientid: String,
    keepalive: Number,
    ipaddress: String,
    proto_ver: Number,
    connected_at: Number,
    disconnect_at: Number,
    device: { type: Schema.Types.ObjectId, ref: "Device" },
});


//格式转换
connectionSchema.methods.toJSONObject = function () {
    return {
        connected: this.connected,
        clientid: this.clientid,
        ipaddress: this.ipaddress,
        connected_at: this.connected_at,
        disconnect_at: this.disconnect_at,
    };
};

const Connection = mongoose.model("Connection", connectionSchema);
module.exports = Connection;

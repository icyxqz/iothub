require("dotenv").config() //配置文件插件
var mongoose = require('mongoose');
var express = require('express');
var deviceRouter = require('./routes/devices');
var webHookeRouter = require('./routes/emqx_web_hook')
var tokensRouter = require("./routes/tokens")
var app = express();

console.log("当前的mongo请求url：",process.env.MONGODB_URL)
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    userUnifiedTopology: true
})


app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/devices', deviceRouter);
app.use("/tokens", tokensRouter);
app.use("/emqx_web_hook",webHookeRouter);


module.exports = app;

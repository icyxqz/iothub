var mqtt = require('mqtt')
var jwt = require('jsonwebtoken')
require('dotenv').config()
console.log('jwt令牌：',process.env.JWT_SECRET)
var username = "yxqz_jwt"
var password = jwt.sign({
    username: username,
    //设置密码有效期1分钟
    exp: Math.floor(Date.now() / 1000) + 60
}, process.env.JWT_SECRET)


var client = mqtt.connect(process.env.MQTT_URL, {
    username: username,
    password: password
})

console.log("当前的临时客户端信息（请求地址）：",client.options.href)
console.log("当前的临时客户端信息（客户端名称）：",client.options.username)
console.log("当前的临时客户端信息（密码：）：",client.options.password)
console.log("当前的临时客户端信息（clientId）：",client.options.clientId)


setTimeout(function () {
    console.log("延迟了3秒钟")
},3000);



client.on('connect', function () {
    console.log("connected")
    client.subscribe("$SYS/brokers/+/clients/+/connected")
    client.subscribe("$SYS/brokers/+/clients/+/disconnected")
})

client.on("message", function (_, message) {
    console.log(message.toString())
})


client.on("error", function (err) {
})
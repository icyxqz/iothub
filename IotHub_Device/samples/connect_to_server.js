require("dotenv").config() //配置文件插件
var IotDevice = require("../sdk/iot_device")
var device = new IotDevice({
    productName: process.env.PRODUCT_NAME,
    deviceName: process.env.DEVICE_NAME,
    secret: process.env.SECRET,
})

device.on("online", function () {
    console.log("设备在线")
})
device.on("offline", function () {
    console.log("设备离线了")
})
device.on("error", function (err) {
    console.log('错误信息：', err)
})

device.connect()